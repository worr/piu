.PHONY = clean all

TESTS = piutest
OBJS = piu.o $(TESTS).o

all: $(OBJS) $(TESTS)

%.o: %.c
	$(CC) $(CFLAGS) -g -c $<

%: %.o piu.o
	$(CC) $(LDFLAGS) -g -o $@ $^

clean:
	rm -f $(OBJS) $(TESTS)
