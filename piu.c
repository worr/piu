#include "piu.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
write_entry(piu *entry, FILE *write_here)
{
	if (fwrite(entry, sizeof(piu), 1, write_here) != 1) {
		perror("fwrite");
	}
}

user *
user_new(int id, char *name, char *uri)
{
	user *ret = malloc(sizeof(user));
	if (ret == NULL)
		return ret;

	ret->user_id = id;
	ret->name = strdup(name);
	if (ret->name == NULL) {
		free(ret);
		return NULL;
	}

	ret->URL_of_avatar = strdup(uri);
	if (ret->URL_of_avatar == NULL) {
		free(ret->name);
		free(ret);
		return NULL;
	}

	ret->when_created = time(NULL);
	if (ret->when_created == (time_t)-1) {
		free(ret->name);
		free(ret);
		return NULL;
	}

	ret->last_activity = time(NULL);
	if (ret->last_activity == (time_t)-1) {
		free(ret->name);
		free(ret);
		return NULL;
	}

	return ret;
}

void
user_free(user *u)
{
	free(u->name);
	free(u->URL_of_avatar);
	free(u);
}

piu *
piu_new(int id, user *u, char *text, unsigned char priv)
{
	piu *ret = malloc(sizeof(piu));
	if (ret == NULL)
		return ret;

	ret->piu_id = id;

	/* XXX: maybe care about these later */
	ret->piu_id_of_repiu = 0;
	ret->user_id_of_repiu = 0;

	if (u != NULL)
		ret->user_id_of_poster = u->user_id;
	ret->poster = u;

	if (strlcpy(ret->piu_text_utf8, text, sizeof(ret->piu_text_utf8)) >= sizeof(ret->piu_text_utf8)) {
		free(ret);
		return NULL;
	}
	ret->piu_length = strlen(text);

	if (ret->poster != NULL) {
		ret->poster->last_activity = time(NULL);
		if (ret->poster->last_activity == (time_t)-1) {
			free(ret);
			return NULL;
		}
	}

	return ret;
}

void
piu_free(piu *p)
{
	free(p);
}

void
surveil(piu *entry)
{
	piu *piu_patt = NULL;
	user *user_patt = NULL;
	surveillance_request *req = NULL;
	int num_users_matched = 0;
	bool matched = false;

	for (int i = 0; i < num_requests; i++) {
		matched = false;
		req = &requests_to_scan[i];
		for (int j = 0; j < req->num_patterns; j++) {
			piu_patt = &req->piu_patterns[j];
			user_patt = &req->user_patterns[j];

			/* The Piu text in the pattern is a substring of the input Piu’s text; AND */
			if (!strstr(piu_patt->piu_text_utf8, entry->piu_text_utf8))
				break;

			/* All ids_following and ids_blocked in the user pattern are followed/blocked by the input Piu’s user; AND */
			num_users_matched = 0;
			for (int k = 0; k < user_patt->num_following; k++) {
				for (int l = 0; l < entry->poster->num_following; l++) {
					if (user_patt->ids_following[k] == entry->poster->ids_following[l]) {
						num_users_matched++;
						break;
					}
				}
			}

			if (num_users_matched != user_patt->num_following)
				break;

			num_users_matched = 0;
			for (int k = 0; k < user_patt->num_blocked; k++) {
				for (int l = 0; l < entry->poster->num_blocked; l++) {
					if (user_patt->ids_blocked[k] == entry->poster->ids_blocked[l]) {
						num_users_matched++;
						break;
					}
				}
			}

			if (num_users_matched != user_patt->num_blocked)
				break;

			/* All of the NONZERO fields in the piu pattern match the input Piu. Values set to zero are “don’t care” inputs. */
			if (piu_patt->piu_id != 0 && piu_patt->piu_id != entry->piu_id)
				break;

			if (piu_patt->piu_id_of_repiu != 0 &&
				piu_patt->piu_id_of_repiu != entry->piu_id_of_repiu)
				break;

			if (piu_patt->user_id_of_repiu != 0 &&
				piu_patt->piu_id_of_repiu != entry->piu_id_of_repiu)
				break;

			if (piu_patt->user_id_of_poster != 0 &&
				piu_patt->user_id_of_poster != entry->user_id_of_poster)
				break;

			if (piu_patt->piu_length != 0 &&
				piu_patt->piu_length != entry->piu_length)
				break;

			if (piu_patt->visible_only_to_followers != 0 &&
				piu_patt->visible_only_to_followers != entry->visible_only_to_followers)
				break;


			matched = true;
		}

		if (matched)
			write_entry(entry, req->write_here);
	}
}

int
preprocess(piu *entry)
{
	surveil(entry);
	/* XXX: commit */
	return 0;
}

