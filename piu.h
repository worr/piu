#ifndef __PIU_H
#define __PIU_H

#include <stdio.h>
#include <time.h>

typedef struct user_struct {
	int user_id;
	time_t when_created;
	time_t last_activity;
	char *name;
	char *URL_of_avatar;

	int num_following, *ids_following;
	int num_blocked, *ids_blocked;
} user;

typedef struct piu_struct {
	int piu_id;
	int piu_id_of_repiu, user_id_of_repiu;	  /* zero if not a re-Piu */

	int user_id_of_poster;
	user *poster;

	char piu_text_utf8[140*4+1];
	unsigned char piu_length;
	unsigned char visible_only_to_followers;
} piu;

typedef struct surveillance_request_struct {
	int id_number;
	int num_patterns;
	user *user_patterns;
	piu *piu_patterns;
	FILE *write_here;
} surveillance_request;

extern int num_requests;
extern surveillance_request *requests_to_scan;

void surveil(piu *entry);
int preprocess(piu *entry);

user *user_new(int id, char *name, char *uri);
void user_free(user *u);

piu *piu_new(int id, user *u, char *text, unsigned char priv);
void piu_free(piu *p);

#endif

