#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include "piu.h"

int num_requests = 1;
surveillance_request *requests_to_scan;

int
main(void)
{
	requests_to_scan = malloc(sizeof(surveillance_request));
	bzero(requests_to_scan, sizeof(surveillance_request));

	requests_to_scan->id_number = 1;
	requests_to_scan->num_patterns = 1;
	requests_to_scan->user_patterns = user_new(1, "worr", "worrbase.com");
	requests_to_scan->piu_patterns = piu_new(1, NULL, "woot", 0);
	requests_to_scan->user_patterns->when_created = 0;
	requests_to_scan->user_patterns->last_activity = 0;

	requests_to_scan->write_here = fopen("foo", "w");
	if (requests_to_scan->write_here == NULL) {
		perror("fopen");
		return EXIT_FAILURE;
	}

	user *worr = user_new(1, "worr", "worrbase.com");
	if (worr == NULL) {
		perror("user_new");
		return EXIT_FAILURE;
	}

	piu *p1 = piu_new(1, worr, "woot", 0);
	if (p1 == NULL) {
		perror("piu_new");
		user_free(worr);
		return EXIT_FAILURE;
	}

	preprocess(p1);

	if (fclose(requests_to_scan->write_here)) {
		perror("fclose");
		user_free(worr);
		piu_free(p1);
		return EXIT_FAILURE;
	}

	user_free(worr);
	piu_free(p1);

	return EXIT_SUCCESS;
}

